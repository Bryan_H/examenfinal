﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenFinal.Entities
{
    public class Prestamo
    {
        public  int id { get; set; }
        public double monto { get; set; }
        public int plazo { get; set; }
        public double interes { get; set; }
        public DateTime date { get; set; }       

        public Prestamo(int id, double monto, int plazo, double interes, DateTime date)
        {
            this.id = id;
            this.monto = monto;
            this.plazo = plazo;
            this.interes = interes;
            this.date = date;
        }

        public Prestamo()
        {
        }
    }
}
