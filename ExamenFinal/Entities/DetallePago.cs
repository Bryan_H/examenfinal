﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenFinal.Entities
{
    public class DetallePago
    {
        public int NCuota { get; set; }
        public DateTime Date { get; set; }
        public double Interes { get; set; }
        public double Principal { get; set; }
        public double Cuota { get; set; }
        public double Pago { get; set; }

        public DetallePago(int nCuota, DateTime date, double interes, double principal, double cuota, double pago)
        {
            NCuota = nCuota;
            Date = date;
            Interes = interes;
            Principal = principal;
            Cuota = cuota;
            Pago = pago;
        }

        public DetallePago()
        {
        }
    }
}
