﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenFinal.util
{
    public class Validator
    {
        public static void Enteros(KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !(e.KeyChar == Convert.ToChar(8)))
            {
                e.Handled = true;
                MessageBox.Show("Este campo solo acepta numeros y ningún espacio", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        public static void Decimales(KeyPressEventArgs v, TextBox t)
        {
            int count = 0;
            if (char.IsDigit(v.KeyChar)) v.Handled = false;
            else if (char.IsSeparator(v.KeyChar)) v.Handled = false;
            else if (char.IsControl(v.KeyChar)) v.Handled = false;
            else if (v.KeyChar.ToString().Equals("."))
            {

                for (int i = 0; i < t.Text.Length; i++)
                {
                    if (t.Text.Substring(i, 1).Equals(".")) ++count;
                }

                count++;

                if (count > 1)
                {
                    v.Handled = true;
                    count = 0;
                }
                else v.Handled = false;
            }
            else v.Handled = true;
        }
    }
}
