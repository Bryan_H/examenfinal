﻿using ExamenFinal.Entities;
using ExamenFinal.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenFinal
{
    public partial class FrmMain : Form
    {
        private int cont;
        private DataSet dsPago;
        private BindingSource bsPago;
        public FrmMain()
        {
            InitializeComponent();
        }

        public DataSet DsPago
        {
            set
            {
                dsPago = value;
            }
        }

        public BindingSource BsPago
        {
            set
            {
                bsPago = value;
            }
        }

        private void txtPrestamo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.Decimales(e, txtPrestamo);
        }

        private bool needWrite()
        {
            if(txtInteres.Text == "" || txtPlazo.Text == "" || txtPrestamo.Text == "")
            {
                return true;
            }

            return false;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (needWrite())
            {
                MessageBox.Show(this,"Es necesaior rellenar todos los campos para continual", "Mensaje de error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            int plazoT = Int32.Parse(txtPlazo.Text) * 12;
            DateTime dt = dtpDate.Value;
            double tazaPeriodoMensual = (Double.Parse(txtInteres.Text) / 100) / 12;

            double cuotaNumerador = Double.Parse(txtPrestamo.Text) * (tazaPeriodoMensual *
                Math.Pow((1 + tazaPeriodoMensual), plazoT));

            double cuotaMensual = cuotaNumerador /
                (Math.Pow((1 + tazaPeriodoMensual), plazoT) - 1);

            double saldo = Double.Parse(txtPrestamo.Text);

            int n = 0;

            for (int i = 1; i <= plazoT; i++)
            {
                cont = cont + 30;
                double interes = saldo * tazaPeriodoMensual;
                double principal = cuotaMensual - interes;
                saldo -= principal;

                DataRow drPrestamo = dsPago.Tables["DetallePago"].NewRow();
                drPrestamo["No.Cuota"] = i;
                drPrestamo["Cuota"] = cuotaMensual;
                drPrestamo["Principal"] = principal;
                drPrestamo["Saldo"] = saldo;
                drPrestamo["Intereses"] = interes;
                drPrestamo["Fecha"] = dt.AddDays(cont);

                dsPago.Tables["DetallePago"].Rows.Add(drPrestamo);
                bsPago.DataSource = dsPago;
                bsPago.DataMember = dsPago.Tables["DetallePago"].TableName;

                dgvPrestamo.DataSource = bsPago;
            }
            btn.Enabled = true;
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            DsPago = dsPrestamo;
            BsPago = bsPrestamo;
            btn.Enabled = false;
        }

        private void btn_Click(object sender, EventArgs e)
        {
            int rowCount = dgvPrestamo.Rows.Count;
           
            if (rowCount <= 0)
            {
                MessageBox.Show(this, "Es necesario calcular los datos", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Report report = new Report();

            for(int i = 0; i < dgvPrestamo.Rows.Count; i++)
            {
                DetallePago p = new DetallePago();

                p.NCuota = (int)this.dgvPrestamo.Rows[i].Cells[0].Value;
                p.Date = (DateTime)this.dgvPrestamo.Rows[i].Cells[1].Value;
                p.Interes = (double)this.dgvPrestamo.Rows[i].Cells[2].Value;
                p.Principal = (double)this.dgvPrestamo.Rows[i].Cells[3].Value;
                p.Cuota = (double)this.dgvPrestamo.Rows[i].Cells[4].Value;
                p.Pago = (double)this.dgvPrestamo.Rows[i].Cells[5].Value;

                report.pagos.Add(p);
            }
            report.Show();

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void txtPlazo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.Enteros(e);
        }

        private void txtInteres_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.Decimales(e, txtInteres);
        }
    }
}
